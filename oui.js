document.addEventListener('DOMContentLoaded', function()
{
  const HEURES = document.querySelector("#heures"),
    MINUTES = document.querySelector("#minutes"),
    SECONDES = document.querySelector("#secondes"),
    HORLOGE = document.querySelector("#horloge"),
    CHIFFRES = document.querySelectorAll(".chiffre");

  var h = 0,
    m = 0,
    s = 0,
    nbh = 0,
    nbm = 0,
    nbs = 0
    testh = true,
    testm = true,
    tests = true,
    d = new Date();
    lasth = 0,
    change = false;

  h = d.getHours();

  if (d >= 12)
  {
    for(i = 0; i < 12; i++)
    {
      CHIFFRES[i].innerText = parseInt(CHIFFRES[i].innerText) + 12;
    }
  }

  function setTime()
  {
    let date = new Date(),
      h = date.getHours(),
      m = date.getMinutes(),
      s = date.getSeconds();

    h = (h > 12 ? h - 12 : h);

    let degh = (h / 12 * 360) - 90,
      degm = (m / 60 * 360) - 90,
      degs = (s / 60 * 360) - 90;

    if (degh == -90 && testh)
    {
      nbh += 1;
      testh = false;
    }
    if (degh > -90 && !testh)
    {
      testh = true;
    }

    if (degm == -90 && testm)
    {
      nbm += 1;
      testm = false;
    }
    if (degm > -90 && !testm)
    {
      testm = true;
    }

    if (degs == -90 && tests)
    {
      nbs += 1;
      tests = false;
    }
    if (degs > -90 && !tests)
    {
      tests = true;
    }

    HEURES.style.setProperty('transform', 'rotate(' + (degh + (360 * nbh)) + 'deg)');
    MINUTES.style.setProperty('transform', 'rotate(' + (degm + (360 * nbm)) + 'deg)');
    SECONDES.style.setProperty('transform', 'rotate(' + (degs + (360 * nbs)) + 'deg)');

    if(h < lasth)
    {
      if(lasth >= 12)
      {
        lasth -= 12;
        change = true;
      }
      else
      {
        change = false;
      }

      CHIFFRES[lasth].innerText = (change ? lasth + 12 : lasth)
    }

    lasth = h;

    setTimeout(()=>
    {
      setTime();
    }, 250)
  }

  setTime();
});
